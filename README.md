# Makan-Makan Outdoor

Dalam rangka untuk mencegah penyebaran virus SARS-CoV-2 (penyebab pandemi Covid-19), maka acara makan-makan yang biasa di dalam lebih baik diadakan di luar.

Repo ini berisi daftar restoran outdoor yang bisa dijadikan tempat makan-makan yang "relatif" aman.

Lihat juga [halaman ini](https://gist.github.com/iamdejan/73f212700e346fd900fa82cb9e1e5116) (dalam bahasa Inggris) untuk petunjuk hidup selama pandemi (protokol kesehatan, metode tes, dll.).

Asumsi: gaji cukup untuk 1x makan Rp. 300 ribu.

## Daftar Isi
- [Banten](#banten)
  * [Bintaro Jaya](#bintaro-jaya)
  * [BSD](#bsd)
  * [Alam Sutera](#alam-sutera)
  * [Ciputat dan Pamulang](#ciputat-dan-pamulang)
- [DKI Jakarta](#dki-jakarta)
  * [Bintaro dan Veteran](#bintaro-dan-veteran)
  * [Cilandak dan Fatmawati](#cilandak-dan-fatmawati)
  * [Menteng dan Cikini](#menteng-dan-cikini)
  * [Setiabudi](#setiabudi)
- [Jawa Barat](#jawa-barat)
  * [Bekasi](#bekasi)
  * [Depok](#depok)
  * [Kota Bogor](#kota-bogor)
  * [Sentul](#sentul)
  * [Puncak](#puncak)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Banten

### Bintaro Jaya

- [Talaga Sampireun](https://g.page/talaga-sampireun-bintaro?share)
    - Style: Sunda.
    - Reservasi: `+62217453999`.
- [Taman Jajan](https://goo.gl/maps/VWjRtZSCa9X6XJND8)
    - Ini kea semacam pusat jajanan, jadi restorannya banyak.
    - **Hati-hati orang merokok**.

### BSD

- [The Leaf Restaurant @ The Quantis Clubhouse](https://goo.gl/maps/FcvedasBqHeUjx5U6)
    - Style: Chinese.
    - Tidak jelas apakah outdoor / tidak, dari foto di Google Maps sih keanya iya, tapi bisa jadi itu kaca.
    - Reservasi: `+6281181188899`.
- [Wang Fu Dimsum](https://goo.gl/maps/FhdsBYEgM95G9X1m7)
    - Style: Chinese (mainly dimsum).
    - Untuk outdoor, 1 meja max. 2 orang dan jumlah terbatas.
    - Gak yakin bisa reservasi / gak, tapi coba tanya aja di `+628111565689`.
- [Resto Gue](https://goo.gl/maps/5iMuFh4U1vWBWpvw8)
    - Style: seafood.
- [May Star - Navapark Country Club](https://goo.gl/maps/6geF4S4dh89wMaJc6)
    - Style: Chinese.
    - Halal kok (kata orang di kolom komentar).
    - Meja outdoor terbatas.
    - Reservasi di `+622153169005`.
- [Bupe Resto (Bukit Pelayanan Resto)](https://goo.gl/maps/iUmppBtBzM1cbEBA8)
    - Style: Sunda.
- [Intro Jazz Bistro](https://goo.gl/maps/krHPHoSQ2BGCy8Rt6)
    - Style: Western? (keanya campur deh)
    - Meja outdoor max. 3 orang dan jumlah terbatas.
    - Reservasi di `+622122230394`.
- [Gubug Berkah Resto](https://goo.gl/maps/umZTCVwE9TJdBnzW9)
    - Style: Sunda.
    - Reservasi di `+6281227000707`.
- [Gubug Makan Mang Engking BSD](https://goo.gl/maps/SF4nNidvV72DwHJNA)
    - Style: Sunda.
    - Reservasi: `+622150385385`.
- [Hygge Signature](https://goo.gl/maps/SF4nNidvV72DwHJNA)
    - Style: Western (banyak beef-nya).
    - Meja outdoor terbatas.
    - **Non-halal** (ada menu pork).
    - Reservasi: `+622150385385`.
- [Bebek Bengil](https://goo.gl/maps/XN53vUg8EfjDtWNXA)
    - Style: Bali.
    - Meja outdoor terbatas.
    - Reservasi: `+622150386630`.

### Alam Sutera
- [Bandar Djakarta](https://goo.gl/maps/MmXzsUWE7ZQ8TSWr9)
    - Style: Sunda.
    - Meja outdoor terbatas.
    - Reservasi: `+622150821990`.

### Ciputat dan Pamulang

- [SHSD Sambel Hejo Sambel Dadak Ciputat](https://goo.gl/maps/9ACVrZ2Hhc4ncQHB7)
    - Meja outdoor terbatas.
    - Reservasi di `+622174775074`.

## DKI Jakarta

### Bintaro dan Veteran

- [RM. Ampera 2 Tak](https://goo.gl/maps/29Y7ZgR8bLT98naT7)
    - Style: Sunda.

### Cilandak dan Fatmawati

 - [Garden Grill et cetera](https://goo.gl/maps/vfvWPZCVfNBnhNZv9)
    - Reservasi: `+628119515152`.

### Menteng dan Cikini

- [Tjikinii Lima](https://g.page/tjikiniilima?share)
    - Style: Nusantara.
    - Reservasi: `+62213900745`.

### Setiabudi

- [Warung MJS](https://goo.gl/maps/85PuR2r3uLVkeJ7h9)
    - Style: Jawa.
    - Meja outdoor terbatas.
    - Reservasi: `+62215252605`.

## Jawa Barat

### Bekasi

- [Rumah Makan Khas Sunda Cibiuk](https://goo.gl/maps/FbHcbcdKVd2BjrZR7)
    - Style: Sunda.
    - Keanya semua meja outdoor?
    - Reservasi: `+622188345134`.

### Depok

- [Rumah Makan Saung Talaga](https://goo.gl/maps/1DzYr4iCPhwzGxEU8)
    - Style: Sunda.
    - Reservasi: `+6281380165357`.

### Kota Bogor

- [Gurih 7 Bogor Indonesia](https://g.page/restogurih7bogor?share)
    - Style: Sunda.
    - Reservasi melalui [WhatsApp](https://api.whatsapp.com/send?phone=628119877010).

### Sentul

- [Talaga Kuring](https://goo.gl/maps/wHm18axVjfuGJ3mo7)
    - Style: Sunda.
    - Reservasi: `+622122930239`.

### Puncak

- [Talaga Sampireun](https://g.page/TalagaSampireunBogor?share)
    - Style: Sunda.
    - Reservasi: `+622518292999`.
- [Kedai Sunda](https://goo.gl/maps/AyBHEr2qFHAsXDpB9)
    - Style: Sunda.
    - Reservasi: `+6285814148488` (sumber: Google Maps) atau `+622518251534` (website resmi [Kedai Sunda](http://www.kedaisunda.com/)).
- [Cimory Riverside](https://goo.gl/maps/6rJZTvHfeXzbDBzq5)
    - Style: Western.
    - Reservasi melalui [WhatsApp](https://api.whatsapp.com/message/ZI2N6HUDNG2ZP1?autoload=1&app_absent=0).
- [Cimory Mountain View](https://goo.gl/maps/tT3qMYKzJNBVHPCeA)
    - Style: Western.
    - Reservasi melalui [WhatsApp](https://api.whatsapp.com/message/ZI2N6HUDNG2ZP1?autoload=1&app_absent=0).
- [Bumi Nini Cisarua](https://g.page/rm-bumi-nini-cisarua?share)
    - Style: Sunda.
    - Reservasi melalui [WhatsApp](https://api.whatsapp.com/send?phone=628111164168) di `+628111164168` atau [melalui website (direkomendasikan)](https://www.bumiaki.com/reservation).
